import {Component, OnInit, OnDestroy, Input, OnChanges, ViewChild} from '@angular/core';
import {Message} from "primeng/components/common/message";
import {Subscription} from "rxjs";
import {Router, ActivatedRoute} from "@angular/router";
import {AuthenticationServicesService} from "../../api-module/services/authentication/authentication-services.service";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";

@Component({
  selector: 'app-call-table',
  templateUrl: './call-table.component.html',
  styleUrls: ['./call-table.component.css']
})

export class CallTableComponent implements OnInit, OnDestroy {
  @Input() calls;
  buttonsList: any[];
  cornerMessage: Message[] = [];
  statusSubscription: Subscription;
  roles: string[];
  content: any;
  activeRow: any;
  newAction: any;
  statuses: any[];

  constructor(private lookUp: LookupService, private modalService: NgbModal, private router: Router, private auth: AuthenticationServicesService, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.calls = [];
    this.buttonsList = [
      {
        label: "Add Action", icon: "fa fa-cog", command: () => {
        this.open(this.content, this.activeRow)
      }
      }
    ];
    this.roles = this.auth.CurrentUser().roles;
    console.log(this.activatedRoute);
  }

  ngOnDestroy() {
    this.statusSubscription && this.statusSubscription.unsubscribe();

  }

  setActiveRow(call, content) {
    this.activeRow = call;
    this.content = content;
  }

  getStatuses() {
    this.statusSubscription = this.lookUp.getStatues().subscribe((status) => {
        this.statuses = status;
      },
      err => {
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get data due to server error"
        })
      })
  }

  open(content, call) {
    this.getStatuses();
    this.newAction = {};
    this.modalService.open(content).result
      .then((result) => {
        if (result) {
          result.fk_Call_Id = call.id;
          result.customerServiceName = this.auth.CurrentUser().fullName;
          result.customerServiceUserName = this.auth.CurrentUser().userName;
          //console.log(result);
          this.lookUp.postNewLog(result).subscribe(() => {
              //console.log('posted successfully');
              this.cornerMessage.push({
                severity: "success",
                summary: "Saved successfully",
                detail: "Action Saved to this call Successfully."
              })
            },
            err => {
              this.cornerMessage.push({
                severity: "error",
                summary: "Failed",
                detail: "Failed Save new Action due to server error"
              })
            });
          //console.log(result);
        }
      });
  }

  routeToLogs(callId) {
    this.router.navigate(['search/calls-history/logs/', callId])
  }

  roueToNewEstimation(callId) {
    this.router.navigate(['search/calls-history/estimation/', callId])
  }
}
