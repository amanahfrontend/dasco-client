import {Component, OnInit, Input} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-existed-customer-contracts',
  templateUrl: './existed-customer-contracts.component.html',
  styleUrls: ['./existed-customer-contracts.component.css']
})
export class ExistedCustomerContractsComponent implements OnInit {
  @Input() id;
  contracts: any[];
  getContractByCustomerIdSubscription: Subscription;

  constructor(private lookup: LookupService) {
  }

  ngOnInit() {
    this.contracts = [];
    this.getContractByCustomerIdSubscription = this.lookup.getContractByCustomerId(this.id).subscribe((contracts) => {
        this.contracts = contracts;
        //console.log(this.contracts)
      },
      err => {
        //console.log(err);
      })
  }

}
