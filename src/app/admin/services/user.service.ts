import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import * as myGlobals from "../../api-module/services/globalPath";

@Injectable()
export class UserService {

  constructor(private readonly http: HttpClient) { }
  
  updateUserPassword(model): Observable<any> {
    var serviceBaseUrl = myGlobals.BaseUrlUserManagement;
    const url = serviceBaseUrl + "User/UpdateUserPassword";
    
    return this.http.put(url, model).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }
}
