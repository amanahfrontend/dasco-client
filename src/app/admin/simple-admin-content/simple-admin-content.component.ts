import {Component, OnInit, Input, OnChanges, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {MessageService} from 'primeng/components/common/messageservice';
import {PromptComponent} from '../../shared-module/shared/prompt/prompt.component'
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";

@Component({
  selector: 'app-simple-admin-content',
  templateUrl: './simple-admin-content.component.html',
  styleUrls: ['./simple-admin-content.component.css']
})
export class SimpleAdminContentComponent implements OnInit, OnChanges, OnDestroy {
  @Input() type: string;
  @Input() getData: string;
  dataSubscription: Subscription;
  deleteDataSubscription: Subscription;
  modalRef: any;
  rows: any[];
  toggleLoading: boolean;

  constructor(private lookUp: LookupService, private messageService: MessageService, private modalService: NgbModal, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    this.rows = [];

    // this.toggleLoading = true;
  }

  ngOnChanges() {
    this.toggleLoading = true;
    !this.rows && (this.rows = []);
    if (this.type == 'Customer Type' && this.getData == 'Customer Type') {
      this.dataSubscription = this.lookUp.getCustomerTypes().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type == 'Call Type' && this.getData == 'Call Type') {
      this.dataSubscription = this.lookUp.getCallsTypes().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type == 'Call Priority' && this.getData == 'Call Priority') {
      this.dataSubscription = this.lookUp.getPriorities().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type == 'Phone Type' && this.getData == 'Phone Type') {
      this.dataSubscription = this.lookUp.getPhoneTypes().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type == 'Action Status' && this.getData == 'Action Status') {
      this.dataSubscription = this.lookUp.getStatues().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type == 'Contract Type' && this.getData == 'Contract Type') {
      this.dataSubscription = this.lookUp.getContractTypes().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type == 'Role' && this.getData == 'Role') {
      this.dataSubscription = this.lookUp.getRoles().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type == 'Order Type' && this.getData == 'Order Type') {
      this.dataSubscription = this.lookUp.getOrderType().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type == 'Order Priority' && this.getData == 'Order Priority') {
      this.dataSubscription = this.lookUp.getOrderPriority().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type == 'Order Status' && this.getData == 'Order Status') {
      this.dataSubscription = this.lookUp.getOrderStatus().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type == 'Order Progress' && this.getData == 'Order Progress') {
      this.dataSubscription = this.lookUp.getAllOrderProgress().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type == 'Order Problems' && this.getData == 'Order Problems') {
      this.dataSubscription = this.lookUp.getAllOrderProblems().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        })
    }
  }

  ngOnDestroy() {
    this.dataSubscription && this.dataSubscription.unsubscribe();
    this.deleteDataSubscription && this.deleteDataSubscription.unsubscribe();
  }

  add() {
    this.openModal({}, `Add New ${this.type}`, this.type);
    this.modalRef.result.then((newValue) => {
      // newValue = {
      //   name: newValue.name
      // };
      console.log(newValue);
      if (this.type == 'Customer Type') {
        this.dataSubscription = this.lookUp.postCustomerTypes(newValue).subscribe((data) => {
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
            this.rows.push(data)
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })

      }
      else if (this.type == 'Call Type') {
        this.dataSubscription = this.lookUp.postCallsTypes(newValue).subscribe((data) => {
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
            this.rows.push(data)
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
      else if (this.type == 'Call Priority') {
        this.dataSubscription = this.lookUp.postCallPriorities(newValue).subscribe((data) => {
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
      else if (this.type == 'Phone Type') {
        this.dataSubscription = this.lookUp.postPhoneTypes(newValue).subscribe((data) => {
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
      else if (this.type == 'Action Status') {
        this.dataSubscription = this.lookUp.postActionStatues(newValue).subscribe((data) => {
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
      else if (this.type == 'Contract Type') {
        this.dataSubscription = this.lookUp.postContractTypes(newValue).subscribe((data) => {
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
      else if (this.type == 'Role') {
        this.dataSubscription = this.lookUp.postRoles(newValue).subscribe((data) => {
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
      else if (this.type == 'Order Type') {
        this.dataSubscription = this.lookUp.postOrderType(newValue).subscribe((data) => {
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
      else if (this.type == 'Order Priority') {
        this.dataSubscription = this.lookUp.postOrderPriority(newValue).subscribe((data) => {
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
      else if (this.type == 'Order Status') {
        this.dataSubscription = this.lookUp.postOrderStatus(newValue).subscribe((data) => {
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
      else if (this.type == 'Order Progress') {
        console.log(newValue);
        this.dataSubscription = this.lookUp.postOrderProgress(newValue).subscribe((data) => {
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
            this.rows.push(data)
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
      else if (this.type == 'Order Problems') {
        console.log(newValue);
        this.dataSubscription = this.lookUp.postOrderProblems(newValue).subscribe((data) => {
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
            this.rows.push(data)
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
    })
      .catch((result) => {
        //console.log('nothing added');
        this.messageService.add({
          severity: 'info',
          summary: 'Nothing Added!',
          detail: "You didn't saved new value."
        });
      });
  }

  edit(row) {
    //console.log(row);
    console.log(row);
    this.openModal(Object.assign({}, row), 'Edit', this.type);
    this.modalRef.result.then((editedValue) => {
      console.log(editedValue);
      // let editedToPost = {
      //   id: row.id,
      //   name: editedValue.name,
      //   fK_OrderStatus_Id: editedValue.fK_OrderStatus_Id
      // };
      // console.log(editedValue);
      if (this.type == 'Customer Type') {
        this.dataSubscription = this.lookUp.updateCustomerTypes(row).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "Edit and Saved Successfully!"
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
      else if (this.type == 'Call Type') {
        this.dataSubscription = this.lookUp.updateCallsTypes(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "Edit and Saved Successfully!"
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
      else if (this.type == 'Call Priority') {
        this.dataSubscription = this.lookUp.updatePriorities(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "Edit and Saved Successfully!"
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
      else if (this.type == 'Phone Type') {
        this.dataSubscription = this.lookUp.updatePhoneTypes(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "Edit and Saved Successfully!"
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
      else if (this.type == 'Action Status') {
        this.dataSubscription = this.lookUp.updateStatues(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "Edit and Saved Successfully!"
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
      else if (this.type == 'Contract Type') {
        this.dataSubscription = this.lookUp.updateContractTypes(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "Edit and Saved Successfully!"
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
      else if (this.type == 'Role') {
        this.dataSubscription = this.lookUp.updateRoles(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            console.log(row);
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "Edit and Saved Successfully!"
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
      else if (this.type == 'Order Type') {
        this.dataSubscription = this.lookUp.updateOrderType(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
      else if (this.type == 'Order Priority') {
        this.dataSubscription = this.lookUp.updateOrderPriority(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
      else if (this.type == 'Order Status') {
        this.dataSubscription = this.lookUp.updateOrderStatus(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
      else if (this.type == 'Order Progress') {
        console.log('Order Progress');
        this.dataSubscription = this.lookUp.updateOrderProgressLookUp(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
      else if (this.type == 'Order Problems') {
        console.log('Order Problems');
        this.dataSubscription = this.lookUp.updateOrderProblemsLookUp(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully! . please refresh your browser to see it."
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
    })
      .catch((result) => {
        this.messageService.add({
          severity: 'info',
          summary: 'Nothing Edited!',
          detail: "You didn't change the old value"
        });
      });
  }

  remove(row) {
    //console.log(row);
    if (this.type == 'Customer Type') {
      this.deleteDataSubscription = this.lookUp.deleteCustomertype(row.id).subscribe(() => {
          // this.rows = data;
          // //console.log(this.rows);
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'Customer Type removed successfully!'
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: 'error',
            summary: 'Failed',
            detail: 'Failed to remove due to server error!'
          });

        })
    }
    else if (this.type == 'Call Type') {
      this.dataSubscription = this.lookUp.deleteCallType(row.id).subscribe((data) => {
          // this.rows = data;
          // //console.log(this.rows);
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'Removed successfully!'
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: 'error',
            summary: 'Failed',
            detail: 'Failed to remove due to server error!'
          });

        })
    }
    else if (this.type == 'Call Priority') {
      this.dataSubscription = this.lookUp.deletePriorities(row.id).subscribe((data) => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'Removed successfully!'
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: 'error',
            summary: 'Failed',
            detail: 'Failed to remove due to server error!'
          });

        })
    }
    else if (this.type == 'Phone Type') {
      this.dataSubscription = this.lookUp.deletePhoneTypes(row.id).subscribe((data) => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'Removed successfully!'
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: 'error',
            summary: 'Failed',
            detail: 'Failed to remove due to server error!'
          });

        })
    }
    else if (this.type == 'Action Status') {
      this.dataSubscription = this.lookUp.deleteStatus(row.id).subscribe((data) => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'Removed successfully!'
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: 'error',
            summary: 'Failed',
            detail: 'Failed to remove due to server error!'
          });

        })
    }
    else if (this.type == 'Contract Type') {
      this.dataSubscription = this.lookUp.deleteContractType(row.id).subscribe((data) => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'Removed successfully!'
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: 'error',
            summary: 'Failed',
            detail: 'Failed to remove due to server error!'
          });

        })
    }
    else if (this.type == 'Role') {
      this.dataSubscription = this.lookUp.deleteRole(row.name).subscribe((data) => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'Removed successfully!'
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: 'error',
            summary: 'Failed',
            detail: 'Failed to remove due to server error!'
          });

        })
    }
    else if (this.type == 'Order Type') {
      this.dataSubscription = this.lookUp.deleteOrderType(row.id).subscribe(() => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'Removed successfully!'
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: 'error',
            summary: 'Failed',
            detail: 'Failed to remove due to server error!'
          });

        })
    }
    else if (this.type == 'Order Priority') {
      this.dataSubscription = this.lookUp.deleteOrderPriority(row.id).subscribe(() => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'Removed successfully!'
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: 'error',
            summary: 'Failed',
            detail: 'Failed to remove due to server error!'
          });

        })
    }
    else if (this.type == 'Order Status') {
      this.dataSubscription = this.lookUp.deleteOrderStatus(row.id).subscribe(() => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'Removed successfully!'
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: 'error',
            summary: 'Failed',
            detail: 'Failed to remove due to server error!'
          });

        })
    }
    else if (this.type == 'Order Progress') {
      this.dataSubscription = this.lookUp.removeProgressStatus(row.id).subscribe(() => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'Removed successfully!'
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: 'error',
            summary: 'Failed',
            detail: 'Failed to remove due to server error!'
          });

        })
    }
    else if (this.type == 'Order Problems') {
      this.dataSubscription = this.lookUp.removeProblem(row.id).subscribe(() => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'Removed successfully!'
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: 'error',
            summary: 'Failed',
            detail: 'Failed to remove due to server error!'
          });

        })
    }
  }

  openModal(data, header, type?) {
    console.log('open Modal');
    this.modalRef = this.modalService.open(PromptComponent);
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.type = type;
    this.modalRef.componentInstance.data = data;
  }

}
