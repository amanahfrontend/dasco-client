import { TestBed, inject } from '@angular/core/testing';

import { AuthenticationServicesService } from './authentication-services.service';

describe('AuthenticationServicesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthenticationServicesService]
    });
  });

  it('should be created', inject([AuthenticationServicesService], (service: AuthenticationServicesService) => {
    expect(service).toBeTruthy();
  }));
});
