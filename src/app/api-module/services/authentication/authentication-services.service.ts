import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions, RequestMethod } from "@angular/http";
import "rxjs/add/operator/map"
import * as myGlobals from "../globalPath";
import { Subject } from "rxjs";
import { HubConnection } from "@aspnet/signalr-client";
import { callHubUrl, quotationHubUrl, orderHubUrl } from "../globalPath";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

let headers = new Headers({ 'Content-Type': "application/json" });
headers.append("Access-Control-Allow-Origin", "*");

let options = new RequestOptions({ headers: headers });

export enum types {
  order = 1,
  call = 2
}

// let options = new RequestOptions( {method: RequestMethod.Post, headers: headers });
@Injectable()

export class AuthenticationServicesService {
  isLoggedin = new BehaviorSubject(<boolean>false);
  userRoles = new Subject<string[]>();
  private _hubConnection: HubConnection;
  quotaionSignalR = new Subject<any>();
  callSignalR = new Subject<any>();
  orderSignalR = new Subject<any>();

  constructor(private http: Http) {
  }


  setLoggedIn(value) {
    // //console.log(value)
    this.isLoggedin.next(value);
  }

  setRoles(value) {
    // //console.log(value)
    this.userRoles.next(value);
  }

  // isLoggedin: boolean = false;

  login(username: string, password: string) {

    const url = myGlobals.BaseUrlUserManagement + "token";

    const params = JSON.stringify({
      userName: username,
      password: password
    });

    return this.http.post(url, params, options).map(
      (response: Response) => {
        // login successful if there's a jwt token in the response  
        let user = response.json();
        // //console.log(user);
        
        if (user && user.id !== "" && user.token.accessToken) {
          //console.log('---------- user login -------------- ');
          //console.log(user);
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem("currentUser", JSON.stringify(user));

          this.setLoggedIn(true);
          this.setRoles(user.roles);
        }

        return user;
      });
  }

  logout() {
    // remove user from local storage to log user out
    if (this.CurrentUser()) {
      this.hubStop();
    }
    localStorage.removeItem("currentUser");
    this.setLoggedIn(false);
  }

  isLoggedIn() {
    const currentUser = localStorage.getItem("currentUser");

    if (currentUser == null) {
      this.setLoggedIn(false);
      return false;
    }
    else {
      return true;
    }
  }

  CurrentUser() {
    const currentUser = localStorage.getItem("currentUser");

    if (currentUser == null /*|| currentUser["token"] == null*/) {
      this.setLoggedIn(false);
      return null;
    }
    else {
      this.setLoggedIn(true);
      this.setRoles(JSON.parse(currentUser).roles);
      return JSON.parse(currentUser);
    }
  }

  activateSignalR() {
    if (this.CurrentUser()) {
      let roles = this.CurrentUser().roles;
      let userId = this.CurrentUser().id;
      let token = "bearer " + this.CurrentUser().token.accessToken;
      console.log(this.CurrentUser());
      if (this.CurrentUser().roles.includes("CallCenter")) {
        this._hubConnection = new HubConnection(quotationHubUrl(userId, roles, token));
        this.hubStart();
      }
      if (this.CurrentUser().roles.includes("Dispatcher")) {
        this._hubConnection = new HubConnection(orderHubUrl(userId, roles, token));
        this.hubStart();
      }
      console.log(this.CurrentUser().roles);
      if (this.CurrentUser().roles.includes("Maintenance")) {
        this._hubConnection = new HubConnection(callHubUrl(userId, roles, token));
        this.hubStart();
      }
    }
  }

  hubStart() {
    if (this.CurrentUser()) {

      this._hubConnection.start()
        .then(() => {
          console.log("started");
          if (this.CurrentUser().roles.includes("CallCenter")) {
            console.log("will push new quotation");
            this._hubConnection.on("onAddQuotation", (data: any) => {
              console.log(data);
              console.log("new quotation pushed");
              this.setQuotaionSignalR(data);
            })
          }
          if (this.CurrentUser().roles.includes("Dispatcher")) {
            console.log("dispatcher");
            this._hubConnection.on("ChangeOrderProgress", (data: any) => {
              console.log(data);
              console.log("new progress order pushed");
              this.setOrderSignalR(data);
            });
            this._hubConnection.on("UpdateOrder", (data: any) => {
              console.log(data);
              console.log("new order update pushed");
              this.setOrderSignalR(data);
            });
            this._hubConnection.on("AssignOrderToDispatcher", (data: any) => {
              console.log(data);
              console.log("new order assigned pushed");
              this.setOrderSignalR(data);
            });
          }
          if (this.CurrentUser().roles.includes("Maintenance")) {
            console.log("will push new call");
            this._hubConnection.on("onAddCall", (data: any) => {
              console.log(data);
              console.log("new call pushed");
              this.setCallSignalR(data);
            })
          }
        })
        .catch((e) => {
          console.log(e);
          console.log("failed");
        })
    }
  }

  hubStop() {
    this._hubConnection && this._hubConnection.stop();
  }

  setQuotaionSignalR(quotation) {
    this.quotaionSignalR.next(quotation)
  }

  setCallSignalR(call) {
    this.callSignalR.next(call)
  }

  setOrderSignalR(order) {
    console.log("order signal R updated");
    this.orderSignalR.next(order)
  }

}
