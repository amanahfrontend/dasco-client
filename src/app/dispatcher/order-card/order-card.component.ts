import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

import {EditOrderModalComponent} from "../../customer/edit-order-modal/edit-order-modal.component"
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SetTimeModalComponent} from "../set-time-modal/set-time-modal.component";
import {TransferDispatcherModalComponent} from "../transfer-dispatcher-modal/transfer-dispatcher-modal.component";
import {OrderProgressModalComponent} from "../order-progress-modal/order-progress-modal.component";
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';

@Component({
  selector: 'app-order-card',
  templateUrl: './order-card.component.html',
  styleUrls: ['./order-card.component.css']
})

export class OrderCardComponent implements OnInit {
  @Input() order: any;
  @Input() bulkAssignMode: any;
  // @Input() isOrderSelected: boolean;
  @Output() orderSelected = new EventEmitter;
  @Output() orderTransfered = new EventEmitter;
  currentDetailedOrderId: any;
  editOrderModalRef: any;
  setTimeModalRef: any;
  openTransferOrder: any;
  openOrderProgress: any;

  constructor(private lookup: LookupService,private modalService: NgbModal) {
  }

  ngOnInit() {
    //if (this.lookup.orderPostData != undefined && Object.keys(this.lookup.orderPostData).length >0) {
    //  this.order = this.lookup.orderPostData;
    //  this.lookup.orderPostData = {};
    //}
  }

  selectOrder() {
    this.orderSelected.emit(this.order);
  }

  setCurrentDetailedOrderId(id) {
    this.currentDetailedOrderId = id;
  }

  openEditOrderModal(data) {
    this.editOrderModalRef = this.modalService.open(EditOrderModalComponent);
    this.editOrderModalRef.componentInstance.data = data;
    this.editOrderModalRef.result
      .then(() => {

      })
      .catch(() => {

      })
  }

  openSetTimeModal(order) {
    this.setTimeModalRef = this.modalService.open(SetTimeModalComponent, {
      size: "sm"
    });
    this.setTimeModalRef.componentInstance.order = order;
    this.setTimeModalRef.result
      .then(() => {
        console.log('set time success');
      })
      .catch(() => {
        console.log('failed');
      })
  }

  openTransfer(order) {
    this.openTransferOrder = this.modalService.open(TransferDispatcherModalComponent, {
      size: "sm"
    });
    this.openTransferOrder.componentInstance.order = order;
    this.openTransferOrder.result
      .then(() => {
        console.log('closed successfully');
        this.orderTransfered.emit(this.order.id);
      })
      .catch(() => {

      })
  }

  openOrderProgressModal(order) {
    this.openOrderProgress = this.modalService.open(OrderProgressModalComponent);
    this.openOrderProgress.componentInstance.order = order;
    this.openOrderProgress.result
      .then(() => {

      })
      .catch(() => {

      })
  }

}
