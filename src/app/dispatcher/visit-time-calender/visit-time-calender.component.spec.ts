import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitTimeCalenderComponent } from './visit-time-calender.component';

describe('VisitTimeCalenderComponent', () => {
  let component: VisitTimeCalenderComponent;
  let fixture: ComponentFixture<VisitTimeCalenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitTimeCalenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitTimeCalenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
